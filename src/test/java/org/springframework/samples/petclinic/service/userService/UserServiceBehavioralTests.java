package org.springframework.samples.petclinic.service.userService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserService;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Arrays;
import java.util.HashSet;

import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class UserServiceBehavioralTests extends AbstractUserServiceTests {
    @Configuration
    public static class ContextConfiguration {
        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }

    @Autowired
    UserService userService;

    @Mock
    User user;

    @Mock
    Role dummyRole1;
    @Mock
    Role dummyRole2;

    @MockBean
    UserRepository userRepository;

    @Test
    public void testSavesUserInRepository() throws Exception {
        when(dummyRole1.getName()).thenReturn("dummy1");
        when(dummyRole2.getName()).thenReturn("ROLE_dummy2");

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(dummyRole1, dummyRole2)));

        userService.saveUser(user);

        verify(userRepository, times(1)).save(user);
    }

    @Test(expected = Exception.class)
    public void testThrowsExceptionWhenUserRolesIsNull() throws Exception {
        when(user.getRoles()).thenReturn(null);
        userService.saveUser(user);
    }

    @Test(expected = Exception.class)
    public void testThrowsExceptionWhenUserRolesIsEmpty() throws Exception {
        when(user.getRoles()).thenReturn(new HashSet<>());
        userService.saveUser(user);
    }

    @Test
    public void testAddsRoleToTheBeginningOfTheRolesOfUser() throws Exception {
        when(dummyRole1.getName()).thenReturn("dummy1");
        when(dummyRole2.getName()).thenReturn("dummy2");

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(dummyRole1, dummyRole2)));

        userService.saveUser(user);

        verify(dummyRole1, times(1)).setName("ROLE_dummy1");
        verify(dummyRole2, times(1)).setName("ROLE_dummy2");
    }

    @Test
    public void testDoesNotAddsRedundantRoleToTheBeginningOfTheRolesOfUser() throws Exception {
        when(dummyRole1.getName()).thenReturn("ROLE_dummy1");
        when(dummyRole2.getName()).thenReturn("ROLE_dummy2");

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(dummyRole1, dummyRole2)));

        userService.saveUser(user);

        verify(dummyRole1, never()).setName(Mockito.anyString());
        verify(dummyRole2, never()).setName(Mockito.anyString());
    }

    @Test
    public void testSetsUserToTheRoleWithNoUser() throws Exception {
        when(dummyRole1.getName()).thenReturn("dummy1");
        when(dummyRole2.getName()).thenReturn("ROLE_dummy2");

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(dummyRole1, dummyRole2)));

        userService.saveUser(user);

        verify(dummyRole1, times(1)).setUser(user);
        verify(dummyRole2, times(1)).setUser(user);
    }

    @Test
    public void testDoesNotSetUserToTheRoleAlreadyHavingUser() throws Exception {
        when(dummyRole1.getName()).thenReturn("dummy1");
        when(dummyRole2.getName()).thenReturn("ROLE_dummy2");

        when(dummyRole1.getUser()).thenReturn(Mockito.mock(User.class));
        when(dummyRole2.getUser()).thenReturn(Mockito.mock(User.class));

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(dummyRole1, dummyRole2)));

        userService.saveUser(user);

        verify(dummyRole1, never()).setUser(Mockito.any(User.class));
        verify(dummyRole2, never()).setUser(Mockito.any(User.class));
    }
}
