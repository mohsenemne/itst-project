package org.springframework.samples.petclinic.service.clinicService;

import com.github.mryf323.tractatus.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.service.ClinicServiceImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Arrays;
import java.util.Collections;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class ClinicServiceVisitOwnerPetsClauseCoverageTests extends ClinicServiceVisitOwnerPetsCoverageTests {
    @Configuration
    public static class ContextConfiguration {}

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )
    @ClauseCoverage(
        predicate = "b.c",
        valuations = {
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "b.c",
        valuations = {
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "d.e",
        valuations = {
            @Valuation(clause = 'd', valuation = false),
            @Valuation(clause = 'e', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "d.e",
        valuations = {
            @Valuation(clause = 'd', valuation = true),
            @Valuation(clause = 'e', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "f",
        valuations = {
            @Valuation(clause = 'f', valuation = true)
        }
    )
    @Test
    public void test1() {
        when(petRepository.findByOwner(owner)).thenReturn(Arrays.asList(
            petAbove3YearsOldAboveAYearFromLastVisit,
            petBelow3YearsOldAboveHalfAYearFromLastVisit,
            petWithoutVisit
        ));

        when(petAbove3YearsOldAboveAYearFromLastVisit.getType()).thenReturn(petType1);
        when(petBelow3YearsOldAboveHalfAYearFromLastVisit.getType()).thenReturn(petType2);
        when(petWithoutVisit.getType()).thenReturn(petType1);

        when(vetRepository.findAll()).thenReturn(Arrays.asList(petType1Vet, petType2Vet));
        clinicService.visitOwnerPets(owner);

        verify(visitRepository, times(3)).save(Mockito.any(Visit.class));
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = true)
        }
    )
    @ClauseCoverage(
        predicate = "a",
        valuations = {
            @Valuation(clause = 'a', valuation = false)
        }
    )
    @ClauseCoverage(
        predicate = "b.c",
        valuations = {
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = false)
        }
    )
    @ClauseCoverage(
        predicate = "b.c",
        valuations = {
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false)
        }
    )
    @ClauseCoverage(
        predicate = "d.e",
        valuations = {
            @Valuation(clause = 'd', valuation = false),
            @Valuation(clause = 'e', valuation = false)
        }
    )
    @ClauseCoverage(
        predicate = "d.e",
        valuations = {
            @Valuation(clause = 'd', valuation = true),
            @Valuation(clause = 'e', valuation = false)
        }
    )
    @ClauseCoverage(
        predicate = "f",
        valuations = {
            @Valuation(clause = 'f', valuation = false)
        }
    )
    @Test(expected = ClinicServiceImpl.VisitException.class)
    public void test2() {
        when(petRepository.findByOwner(owner)).thenReturn(Arrays.asList(
            petAbove3YearsOldBelowAYearFromLastVisit,
            petBelow3YearsOldBelowHalfAYearFromLastVisit,
            petWithoutVisit
        ));

        when(petWithoutVisit.getType()).thenReturn(petType1);

        when(vetRepository.findAll()).thenReturn(Collections.singletonList(petType2Vet));
        clinicService.visitOwnerPets(owner);

        verify(visitRepository, times(0)).save(Mockito.any(Visit.class));
    }
}
