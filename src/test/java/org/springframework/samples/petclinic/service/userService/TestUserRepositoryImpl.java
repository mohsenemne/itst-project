package org.springframework.samples.petclinic.service.userService;

import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.Repository;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;

public class TestUserRepositoryImpl implements UserRepository {
    @Override
    public void save(User user) throws DataAccessException {}
}
