package org.springframework.samples.petclinic.service.clinicService;

import com.github.mryf323.tractatus.CACC;
import com.github.mryf323.tractatus.ClauseDefinition;
import com.github.mryf323.tractatus.Valuation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.samples.petclinic.model.Visit;
import org.springframework.samples.petclinic.service.ClinicServiceImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Calendar;
import java.util.Collections;
import java.util.Optional;

import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class ClinicServiceVisitOwnerPetsCACCoverageTests extends ClinicServiceVisitOwnerPetsCoverageTests {
    @Configuration
    public static class ContextConfiguration {}

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = true),
        },
        predicateValue = true
    )
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'b',
        valuations = {
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false),
            @Valuation(clause = 'e', valuation = true),
        },
        predicateValue = true
    )
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'c',
        valuations = {
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = false),
            @Valuation(clause = 'e', valuation = true),
        },
        predicateValue = true
    )
    @CACC(
        predicate = "f",
        majorClause = 'f',
        valuations = {
            @Valuation(clause = 'f', valuation = true),
        },
        predicateValue = true
    )
    @Test
    public void CACCoverageTest1() {
        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(
            petAbove3YearsOldAboveAYearFromLastVisit
        ));

        when(vetRepository.findAll()).thenReturn(Collections.singletonList(petType1Vet));
        when(petAbove3YearsOldAboveAYearFromLastVisit.getType()).thenReturn(petType1);

        clinicService.visitOwnerPets(owner);

        verify(visitRepository, times(1)).save(Mockito.any(Visit.class));
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "a",
        majorClause = 'a',
        valuations = {
            @Valuation(clause = 'a', valuation = false),
        },
        predicateValue = false
    )
    @CACC(
        predicate = "f",
        majorClause = 'f',
        valuations = {
            @Valuation(clause = 'f', valuation = false),
        },
        predicateValue = false
    )
    @Test(expected = ClinicServiceImpl.VisitException.class)
    public void CACCoverageTest2() {
        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(petWithoutVisit));

        when(vetRepository.findAll()).thenReturn(Collections.singletonList(petType1Vet));
        when(petWithoutVisit.getType()).thenReturn(petType2);

        clinicService.visitOwnerPets(owner);
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'c',
        valuations = {
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false),
            @Valuation(clause = 'e', valuation = false),
        },
        predicateValue = false
    )
    @Test
    public void CACCoverageTest3() {
        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(
            petAbove3YearsOldBelowAYearFromLastVisit
        ));

        clinicService.visitOwnerPets(owner);

        verify(visitRepository, times(0)).save(Mockito.any(Visit.class));
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'd',
        valuations = {
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = true),
            @Valuation(clause = 'e', valuation = true),
        },
        predicateValue = true
    )
    @Test(expected = ClinicServiceImpl.VisitException.class)
    public void CACCoverageTest4() {
        Visit lastVisit = mock(Visit.class);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -200);
        when(lastVisit.getDate()).thenReturn(calendar.getTime());
        when(petBelow3YearsOldAboveHalfAYearFromLastVisit.getLastVisit()).thenReturn(Optional.of(lastVisit));

        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(
            petBelow3YearsOldAboveHalfAYearFromLastVisit
        ));

        clinicService.visitOwnerPets(owner);
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'd',
        valuations = {
            @Valuation(clause = 'b', valuation = true),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = false),
            @Valuation(clause = 'e', valuation = true),
        },
        predicateValue = false
    )
    @Test
    public void CACCoverageTest5() {
        Visit lastVisit = mock(Visit.class);
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, -200);
        when(lastVisit.getDate()).thenReturn(calendar.getTime());
        when(petAbove3YearsOldBelowAYearFromLastVisit.getLastVisit()).thenReturn(Optional.of(lastVisit));

        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(
            petAbove3YearsOldBelowAYearFromLastVisit
        ));

        clinicService.visitOwnerPets(owner);

        verify(visitRepository, times(0)).save(Mockito.any(Visit.class));
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'e',
        valuations = {
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = true),
            @Valuation(clause = 'd', valuation = true),
            @Valuation(clause = 'e', valuation = true),
        },
        predicateValue = true
    )
    @Test(expected = ClinicServiceImpl.VisitException.class)
    public void CACCoverageTest6() {
        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(
            petBelow3YearsOldAboveHalfAYearFromLastVisit
        ));

        clinicService.visitOwnerPets(owner);
    }

    @ClauseDefinition(clause = 'a', def = "last.isPresent()")
    @ClauseDefinition(clause = 'b', def = "age>3")
    @ClauseDefinition(clause = 'c', def = "daysFromLastVisit>364")
    @ClauseDefinition(clause = 'd', def = "age<=3")
    @ClauseDefinition(clause = 'e', def = "daysFromLastVisit>182")
    @ClauseDefinition(clause = 'f', def = "vet.isPresent()")
    @CACC(
        predicate = "b.c + d.e",
        majorClause = 'e',
        valuations = {
            @Valuation(clause = 'b', valuation = false),
            @Valuation(clause = 'c', valuation = false),
            @Valuation(clause = 'd', valuation = true),
            @Valuation(clause = 'e', valuation = false),
        },
        predicateValue = false
    )
    @Test
    public void CACCoverageTest7() {
        when(petRepository.findByOwner(owner)).thenReturn(Collections.singletonList(
            petBelow3YearsOldBelowHalfAYearFromLastVisit
        ));

        clinicService.visitOwnerPets(owner);

        verify(visitRepository, times(0)).save(Mockito.any(Visit.class));
    }
}
