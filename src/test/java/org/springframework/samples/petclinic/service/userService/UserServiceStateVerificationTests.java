package org.springframework.samples.petclinic.service.userService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.samples.petclinic.model.Role;
import org.springframework.samples.petclinic.model.User;
import org.springframework.samples.petclinic.repository.UserRepository;
import org.springframework.samples.petclinic.service.UserService;
import org.springframework.samples.petclinic.service.UserServiceImpl;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.util.AssertionErrors.assertEquals;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader = AnnotationConfigContextLoader.class)
public class UserServiceStateVerificationTests extends AbstractUserServiceTests {

    @Configuration
    public static class ContextConfiguration {
        @Bean
        public UserRepository userRepository() {
            return new TestUserRepositoryImpl();
        }

        @Bean
        public UserService userService() {
            return new UserServiceImpl();
        }
    }

    @Autowired
    UserService userService;

    @Mock
    User user;

    @Test(expected = Exception.class)
    public void testThrowsExceptionWhenUserRolesIsNull() throws Exception {
        when(user.getRoles()).thenReturn(null);
        userService.saveUser(user);
    }

    @Test(expected = Exception.class)
    public void testThrowsExceptionWhenUserRolesIsEmpty() throws Exception {
        when(user.getRoles()).thenReturn(new HashSet<>());
        userService.saveUser(user);
    }

    @Test
    public void testAddsRoleToTheBeginningOfTheRolesOfUser() throws Exception {
        Role testRole1 = Mockito.spy(Role.class);
        testRole1.setName("test-1");
        Role testRole2 = Mockito.spy(Role.class);
        testRole2.setName("test-2");

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(testRole1, testRole2)));

        userService.saveUser(user);

        assertThat(user.getRoles().parallelStream().allMatch(role -> role.getName().startsWith("ROLE_")), is(true));
    }

    @Test
    public void testDoesNotAddsRedundantRoleToTheBeginningOfTheRolesOfUser() throws Exception {
        Role testRole1 = Mockito.spy(Role.class);
        testRole1.setName("ROLE_test-1");
        Role testRole2 = Mockito.spy(Role.class);
        testRole2.setName("ROLE_test-2");

        when(user.getRoles()).thenReturn(new HashSet<>(Arrays.asList(testRole1, testRole2)));

        userService.saveUser(user);

        assertThat(user.getRoles().parallelStream().allMatch(role -> role.getName().startsWith("ROLE_ROLE_")), is(false));
    }

    @Test
    public void testSetsUserToTheRoleWithNoUser() throws Exception {
        Role testRole = Mockito.spy(Role.class);
        testRole.setName("test");

        when(user.getRoles()).thenReturn(new HashSet<>(Collections.singletonList(testRole)));

        userService.saveUser(user);

        assertEquals("Didn't set user to the role whit no user", user, testRole.getUser());
    }

    @Test
    public void testDoesNotSetUserToTheRoleAlreadyHavingUser() throws Exception {
        Role testRole = Mockito.spy(Role.class);
        testRole.setName("test");
        User dummyUser = Mockito.mock(User.class);
        testRole.setUser(dummyUser);

        when(user.getRoles()).thenReturn(new HashSet<>(Collections.singletonList(testRole)));

        userService.saveUser(user);

        assertEquals("Overrided roles user", dummyUser, testRole.getUser());
    }
}

