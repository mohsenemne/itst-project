package org.springframework.samples.petclinic.service.clinicService;

import org.junit.Before;
import org.mockito.Mock;
import org.mockito.Spy;
import org.springframework.samples.petclinic.model.*;
import org.springframework.samples.petclinic.repository.*;
import org.springframework.samples.petclinic.service.ClinicService;
import org.springframework.samples.petclinic.service.ClinicServiceImpl;

import java.util.Calendar;
import java.util.Optional;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


abstract public class ClinicServiceVisitOwnerPetsCoverageTests {
    @Spy
    protected ClinicService clinicService;
    @Mock
    protected VisitRepository visitRepository;

    @Before
    public void initialize() {
        clinicService = new ClinicServiceImpl(petRepository,
            vetRepository,
            mock(OwnerRepository.class),
            visitRepository,
            mock(SpecialtyRepository.class),
            mock(PetTypeRepository.class)
        );

        Calendar calendar = Calendar.getInstance();

        calendar.add(Calendar.YEAR, -1);
        when(petBelow3YearsOldBelowHalfAYearFromLastVisit.getBirthDate()).thenReturn(calendar.getTime());
        when(petBelow3YearsOldAboveHalfAYearFromLastVisit.getBirthDate()).thenReturn(calendar.getTime());

        calendar.add(Calendar.YEAR, -3);
        when(petAbove3YearsOldBelowAYearFromLastVisit.getBirthDate()).thenReturn(calendar.getTime());
        when(petAbove3YearsOldAboveAYearFromLastVisit.getBirthDate()).thenReturn(calendar.getTime());

        calendar.add(Calendar.YEAR, 4);

        Visit visitWithDateAbove1Year = mock(Visit.class);
        Visit visitWithDateBelow1Year = mock(Visit.class);
        calendar.add(Calendar.DATE, -500);
        when(visitWithDateAbove1Year.getDate()).thenReturn(calendar.getTime());
        when(petBelow3YearsOldAboveHalfAYearFromLastVisit.getLastVisit()).thenReturn(Optional.of(visitWithDateAbove1Year));
        when(petAbove3YearsOldAboveAYearFromLastVisit.getLastVisit()).thenReturn(Optional.of(visitWithDateAbove1Year));

        calendar.add(Calendar.DATE, 400);
        when(visitWithDateBelow1Year.getDate()).thenReturn(calendar.getTime());
        when(petBelow3YearsOldBelowHalfAYearFromLastVisit.getLastVisit()).thenReturn(Optional.of(visitWithDateBelow1Year));
        when(petAbove3YearsOldBelowAYearFromLastVisit.getLastVisit()).thenReturn(Optional.of(visitWithDateBelow1Year));

        when(petWithoutVisit.getLastVisit()).thenReturn(Optional.empty());

        when(petType1Vet.canCurePetTye(petType1)).thenReturn(true);
        when(petType2Vet.canCurePetTye(petType2)).thenReturn(true);
    }

    @Mock
    protected Pet petAbove3YearsOldAboveAYearFromLastVisit;
    @Mock
    protected Pet petBelow3YearsOldAboveHalfAYearFromLastVisit;
    @Mock
    protected Pet petAbove3YearsOldBelowAYearFromLastVisit;
    @Mock
    protected Pet petBelow3YearsOldBelowHalfAYearFromLastVisit;
    @Mock
    protected Pet petWithoutVisit;

    @Mock
    protected PetType petType1;
    @Mock
    protected PetType petType2;

    @Mock
    protected Vet petType1Vet;
    @Mock
    protected Vet petType2Vet;

    @Mock
    protected Owner owner;

    @Mock
    protected PetRepository petRepository;

    @Mock
    protected VetRepository vetRepository;
}
