package org.springframework.samples.petclinic.model;

import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

import java.lang.reflect.Field;
import java.util.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(Theories.class)
public class OwnerGetPetTests {
    private static Owner owner;

    private static Pet pet1 = createPet(1, "poppy");
    private static Pet pet2 = createPet(2, "bella");
    private static Pet pet3 = createPet(3, "molly");
    private static Pet pet4 = createPet(4, "alfie");
    private static Pet pet5 = createPet(null, "teddy");

    private static Pet createPet(Integer id, String name) {
        Pet pet = new Pet();
        pet.setId(id);
        pet.setName(name);
        pet.setBirthDate(new Date());
        pet.setOwner(owner);
        return pet;
    }

    @Before
    public void initialize() {
        owner = new Owner();
    }

    @DataPoints
    public static Pet[] pets = new Pet[] {pet1, pet2, pet3, pet4, pet5};

    @DataPoints
    public static Set[] petSets = new Set[]{
        new HashSet<>(Collections.singletonList(pet5)),
        new HashSet<>(Arrays.asList(pet1, pet2, pet4)),
        new HashSet<>(Collections.emptyList()),
        new HashSet<>(Arrays.asList(pet3, pet4)),
        new HashSet<>(Arrays.asList(pet1, pet2, pet3, pet4, pet5)),
    };

    @DataPoints
    public static boolean[] ignoreNew = new boolean[] {true, false};

    @Theory
    public void testGetPet(Set<Pet> pets, Pet pet, boolean ignoreNew) throws NoSuchFieldException, IllegalAccessException {
        final Field petsField = owner.getClass().getDeclaredField("pets");
        petsField.setAccessible(true);

        petsField.set(owner, pets);
        Pet expectedOutput = (pets.contains(pet) && (!ignoreNew || !pet.isNew())) ? pet : null;

        String message = "Unexpected output on input (pets=" + pets + ", pet=" + pet + ", ignoreNew" + ignoreNew + ")";
        assertEquals(message, expectedOutput, owner.getPet(pet.getName(), ignoreNew));
    }

    @Theory
    public void testGetPetWithOneParameter(Set<Pet> pets, Pet pet) throws IllegalAccessException, NoSuchFieldException {
        final Field petsField = owner.getClass().getDeclaredField("pets");
        petsField.setAccessible(true);

        petsField.set(owner, pets);
        Pet expectedOutput = owner.getPet(pet.getName(), false);

        String message = "Unexpected output on input (pets=" + pets + ", pet=" + pet + ")";
        assertEquals(message, expectedOutput, owner.getPet(pet.getName()));
    }
}
