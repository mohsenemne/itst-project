package org.springframework.samples.petclinic.model;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.lang.reflect.Field;
import java.util.*;

import static org.springframework.test.util.AssertionErrors.assertEquals;

@RunWith(Parameterized.class)
public class OwnerGetPetsTests {
    private static Owner owner;

    private Set<Pet> inputPets;
    private List<Pet> expectedResult;

    private static PetType createPetType(Integer id, String name) {
        PetType petType = new PetType();
        petType.setId(id);
        petType.setName(name);
        return petType;
    }

    private static Pet createPet(Integer id, String name, PetType petType) {
        Pet pet = new Pet();
        pet.setId(id);
        pet.setName(name);
        pet.setBirthDate(new Date());
        pet.setOwner(owner);
        pet.setType(petType);
        return pet;
    }

    @Before
    public void initOwner() {
        owner = new Owner();
    }

    public OwnerGetPetsTests(Set<Pet> inputPets, List<Pet> expectedResult) {
        this.inputPets = inputPets;
        this.expectedResult = expectedResult;
    }

    @Parameterized.Parameters
    public static List<Object> petSets() {
        PetType petTypeCat = createPetType(1, "cat");
        PetType petTypeDog = createPetType(2, "dog");
        PetType petTypeRat = createPetType(3, "rat");

        Pet pet1 = createPet(1, "poppy", petTypeCat);
        Pet pet2 = createPet(2, "bella", petTypeDog);
        Pet pet3 = createPet(3, "molly", petTypeRat);
        Pet pet4 = createPet(4, "alfie", petTypeCat);
        Pet pet5 = createPet(null, "teddy", petTypeDog);

        return Arrays.asList(new Object[][]{
            {new HashSet<>(Collections.singletonList(pet5)), Collections.singletonList(pet5)},
            {new HashSet<>(Arrays.asList(pet3, pet4, pet5)), Arrays.asList(pet4, pet3, pet5)},
            {new HashSet<>(), Collections.emptyList()},
            {new HashSet<>(Arrays.asList(pet2, pet3, pet4, pet5)), Arrays.asList(pet4, pet2, pet3, pet5)},
            {new HashSet<>(Arrays.asList(pet1, pet2, pet3, pet4, pet5)), Arrays.asList(pet4, pet2, pet3, pet1, pet5)},
        });
    }

    @Test(expected = UnsupportedOperationException.class)
    public void testGetPets() throws IllegalAccessException, NoSuchFieldException {
        final Field petsField = owner.getClass().getDeclaredField("pets");
        petsField.setAccessible(true);

        petsField.set(owner, inputPets);
        List<Pet> pets = owner.getPets();
        assertEquals("Field \"pets\" wasn't retrieved properly", expectedResult, pets);
        pets.add(null);
    }
}
