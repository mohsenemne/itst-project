package org.springframework.samples.petclinic.model;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

import static org.springframework.test.util.AssertionErrors.assertEquals;

public class OwnerTests {
    private Owner owner;

    @Before
    public void initOwner() {
        owner = new Owner();
    }

    @Test
    public void testGetAddress() throws IllegalAccessException, NoSuchFieldException {
        final Field addressField = owner.getClass().getDeclaredField("address");
        addressField.setAccessible(true);
        addressField.set(owner, "test_address");

        assertEquals("Field \"address\" wasn't retrieved properly", "test_address", owner.getAddress());
    }

    @Test
    public void testSetAddress() throws IllegalAccessException, NoSuchFieldException {
        owner.setAddress("test_address");

        final Field addressField = owner.getClass().getDeclaredField("address");
        addressField.setAccessible(true);
        assertEquals("Field \"address\" didn't match", "test_address", addressField.get(owner));
    }

    @Test
    public void testGetCity() throws IllegalAccessException, NoSuchFieldException {
        final Field cityField = owner.getClass().getDeclaredField("city");
        cityField.setAccessible(true);
        cityField.set(owner, "test_city");

        assertEquals("Field \"city\" wasn't retrieved properly", "test_city", owner.getCity());
    }

    @Test
    public void testSetCity() throws IllegalAccessException, NoSuchFieldException {
        owner.setCity("test_city");

        final Field cityField = owner.getClass().getDeclaredField("city");
        cityField.setAccessible(true);
        assertEquals("Field \"city\" didn't match", "test_city", cityField.get(owner));
    }

    @Test
    public void testGetTelephone() throws IllegalAccessException, NoSuchFieldException {
        final Field telephoneField = owner.getClass().getDeclaredField("telephone");
        telephoneField.setAccessible(true);
        telephoneField.set(owner, "test_telephone");

        assertEquals("Field \"telephone\" wasn't retrieved properly", "test_telephone", owner.getTelephone());
    }

    @Test
    public void testSetTelephone() throws IllegalAccessException, NoSuchFieldException {
        owner.setTelephone("test_telephone");

        final Field telephoneField = owner.getClass().getDeclaredField("telephone");
        telephoneField.setAccessible(true);
        assertEquals("Field \"telephone\" didn't match", "test_telephone", telephoneField.get(owner));
    }

    @Test(expected=NullPointerException.class)
    public void testAddNullPet() {
        owner.addPet(null);
    }

    private PetType createPetType(Integer id, String name) {
        PetType petType = new PetType();
        petType.setId(id);
        petType.setName(name);
        return petType;
    }

    private Pet createPet(Integer id, String name, PetType petType) {
        Pet pet = new Pet();
        pet.setId(id);
        pet.setName(name);
        pet.setBirthDate(new Date());
        pet.setOwner(owner);
        pet.setType(petType);
        return pet;
    }

    @Test
    public void testAddNotNullPetToEmptySet() throws NoSuchFieldException, IllegalAccessException {
        PetType testPetType = createPetType(1, "test_pet_type");
        Pet testPet = createPet(1, "test_pet", testPetType);

        final Field petsField = owner.getClass().getDeclaredField("pets");
        petsField.setAccessible(true);

        owner.addPet(testPet);
        assertEquals("Field \"pets\" didn't match", new HashSet<>(Collections.singletonList(testPet)), petsField.get(owner));
        assertEquals("Pet's field \"owner\" didn't match", owner, testPet.getOwner());
    }

    @Test
    public void testAddNotNullPetToNotEmptySet() throws NoSuchFieldException, IllegalAccessException {
        PetType testPetType = createPetType(1, "test_pet_type");
        Pet testPet1 = createPet(1, "test_pet_1", testPetType);
        Pet testPet2 = createPet(2, "test_pet_2", testPetType);

        final Field petsField = owner.getClass().getDeclaredField("pets");
        petsField.setAccessible(true);

        petsField.set(owner, new HashSet<>(Collections.singletonList(testPet1)));

        owner.addPet(testPet2);
        assertEquals("Field \"pets\" didn't match", new HashSet<>(Arrays.asList(testPet1, testPet2)), petsField.get(owner));
        assertEquals("Pet's field \"owner\" didn't match", owner, testPet1.getOwner());
        assertEquals("Pet's field \"owner\" didn't match", owner, testPet2.getOwner());
    }
}
