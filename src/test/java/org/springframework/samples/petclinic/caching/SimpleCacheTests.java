package org.springframework.samples.petclinic.caching;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.persistence.EntityManager;

import static org.mockito.Mockito.*;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class SimpleCacheTests {
    @Before
    public void initMocks() {
        simpleCache = new SimpleCache(entityManager);
    }

    @Mock
    EntityManager entityManager;

    private SimpleCache simpleCache;

    @Test
    public void testCacheMissesWhenMapIsEmpty() {
        Object id = Mockito.mock(Object.class);
        Class<Object> clazz = Object.class;

        simpleCache.retrieve(clazz, id);
        verify(entityManager, times(1)).find(clazz, id);
    }

    @Test
    public void testCacheSavesWhenMissOccurs() {
        Object id = Mockito.mock(Object.class);
        Class<Object> clazz = Object.class;

        Object mockedObject = Mockito.mock(Object.class);
        when(entityManager.find(clazz, id)).thenReturn(mockedObject);

        simpleCache.retrieve(clazz, id);
        simpleCache.retrieve(clazz, id);
        verify(entityManager, times(1)).find(clazz, id);
    }

    @Test
    public void testEvictRemovesKeyFromCache() {
        Object id = Mockito.mock(Object.class);
        Class<Object> clazz = Object.class;

        Object mockedObject = Mockito.mock(Object.class);
        when(entityManager.find(clazz, id)).thenReturn(mockedObject);

        simpleCache.retrieve(clazz, id);
        simpleCache.retrieve(clazz, id);
        simpleCache.evict(clazz, id);
        simpleCache.retrieve(clazz, id);
        verify(entityManager, times(2)).find(clazz, id);
    }
}
